## Porocilo 2 pri predmetu Preizkušanje programske opreme
> Kristjan Žagar, E1095120

> Izvorna koda: [Gitlab link](https://gitlab.com/Kikisaki/arrayop)
***
## Implementirane funkcije
|Predpis funkcije | Opis | 
|:--- | :--- | 
| `izpis(self)` | funkcija izpiše 2D ali 3D tabelo ustrezno poravnano |
| `mergeSort(self, vrstice)` | funkcija sortira 1D ali 2D tabelo z algoritmom merge sort, če je `vrstice` true, bo sotriral po vrsticah, drugače pa po stolpcih |
| `iskanje(self, isk)` | kot parameter `isk` prejme šteivlo, ki ga iščemo, vrne pa koordinate, kjer je ta najden  |
| `__add__ (self, other)` | prekrita funkcija za seštevanje, deluje med matrikami in s skalarjem |
| `__radd__ (self, other)` | da deluje seštevanje obojestransko  |
| `__mul__ (self, other)` | prekrita funkcija za množenje, deluje med matrikami in s skalarjem, če sta dve matriki se izvede matrično množenje  |
| `__rmul__ (self, other)` |  da deluje množenje obojestransko  |
| `def __sub__ (self, other): ` | prekrita funkcija za odštevanje, deluje med matrikami in s skalarjem  |
| `__rsub__(self, other)` | da deluje odštevanje v obe smeri pravilno  |
| `__truediv__(self, other)` | prekrita funkcija za deljenje, deluje med matrikami in s skalarjem  |
| `log (self, base)` | implementirana funkcija za logaritmiranje, računa z osnovo, ki jo podamo v parameter `base`  |
| `exp (self)` | funkcija za računanje exponenta, uporablja funkcijo `math.exp` nad vsemi elemnti v matrki  |
| `merge_sort_function(arr)` | pomožna funkcija za merge sort |

***
***
## Implementirani testi
| Ime testa | Opis testa | 
|:--- | :--- | 
| `test_izpis_int_poz_povrsti` | testiram, če pravilno izpiše 2D polje intov|
| `test_izpis_int_poz_ne_povrsti` | test je ostal, samo da preveri, ali se slučajno elemnti ne sortirajo sami, kar je povzročal { } probleme |
| `test_izpis_int_negs` | testiram, če se pravilno negativne vrednosti izpišejo |
| `test_izpis_float` | testiram, če se pravilno števila z plavajočo vejico izpišejo |
| `test_izpis_3D` | testiram, če se ustrezno izpiše 3D matrika |
| `test_MergeSort_1D` | testiram ali merge sort deluje pravilno v 1D matriki |
| `test_MergeSort_stolpci` | testiram, če merge po stolpcih sort deluje pravilno v 2D matriki |
| `test_MergeSort_vrstice` | testiram, če merge sort po vrsticah deluje pravilno v 2D matriki |
| `test_iskanje_1D` | testiram, če metoda za iskanje pravilno išče in najde elemente v 1D seznamu |
| `test_iskanje_2D` | testiram, če metoda za iskanje pravilno išče in najde elemente v 2D seznamu |
| `test_iskanje_3D` | testiram, če metoda za iskanje pravilno išče in najde elemente v 3D seznamu |
| `test_iskanje_1D_shouldFail` | testiram, če metoda javi pravilno, če ni nobenega najdenega elementa |
| `test_add` | testiram, če je seštevanje s skalarjem praivlno |
| `test_sub` | testiram, če je odštevanje s skalarjem praivlno |
| `test_div` | testiram, če je deljenje s skalarjem praivlno |
| `test_mul` | testiram, če je množenje s skalarjem praivlno |
| `test_add_2` | testiram, če je seštevanje z drugo matriko praivlno |
| `test_sub_2` | testiram, če je odštevanje z drugo matriko praivlno |
| `test_div_2` | testiram, če je deljenje z drugo matriko praivlno |
| `test_mul_2` | testiram, če je matrično množenje z drugo matriko praivlno |
| `test_napacne_dimenzije` | testiram, če pravilno javi napako, če matriki nista enakih dimenzij |
| `test_add_3D` | testiram, če tudi deluje na 3D matriki |
| `test_log` | testiram, če logaritmiranje deluje pravilno |
| `test_exp` | testiram, če eksponentiranje deluje pravilno |

## **Vsi testi so uspešni!**