Prvo porocilo
=

V projekt sem dodal novi remote branch, ter pullal spremembe.
Prišlo je do nekaj konfliktov, ki sem jih s pomočjo pycharma
razrešil. Ko sem zagnal teste so bili nekateri neuspešni, zaradi spremembe
dimenzij arrayev. Spodaj sem označil kateri testi so neuspešni, recimo:

```py
Traceback (most recent call last):
  File "D:\School\3_Letnik\Preizkusanje\arrayop\tests\myarray\test_array.py", line 371, in test_iskanje_3D
    self.assertEqual(capturedOutput.getvalue(), exp)
AssertionError: '\nFound at indexses: (1,1,1), (2,1,1), (2,1,2), \n' != '\nFound at indexses: (0,0,0), (1,0,0), (1,0,1), \n'

- Found at indexses: (1,1,1), (2,1,1), (2,1,2),
+ Found at indexses: (0,0,0), (1,0,0), (1,0,1),
```

Celotno porocilo:

```py
PS D:\School\3_Letnik\Preizkusanje\arrayop> python.exe -m unittest tests.myarray.test_array.TestArray -v
test_MergeSort_1D (tests.myarray.test_array.TestArray) ... ok
test_MergeSort_stolpci (tests.myarray.test_array.TestArray) ... ok
test_MergeSort_vrstice (tests.myarray.test_array.TestArray) ... ok
test___init__ (tests.myarray.test_array.TestArray) ... ok
test_add (tests.myarray.test_array.TestArray) ... ok
test_add_2 (tests.myarray.test_array.TestArray) ... ok
test_add_3D (tests.myarray.test_array.TestArray) ... ok
test_contains (tests.myarray.test_array.TestArray) ... ok
test_div (tests.myarray.test_array.TestArray) ... ok
test_div_2 (tests.myarray.test_array.TestArray) ... ok
test_dtype (tests.myarray.test_array.TestArray) ... ok
test_exp (tests.myarray.test_array.TestArray) ... ok
test_get_set_item (tests.myarray.test_array.TestArray) ... ok
test_is_valid_instance (tests.myarray.test_array.TestArray) ... ok
#test_iskanje_1D (tests.myarray.test_array.TestArray) ... ERROR
#test_iskanje_1D_shouldFail (tests.myarray.test_array.TestArray) ... ERROR
#test_iskanje_2D (tests.myarray.test_array.TestArray) ... ERROR
#test_iskanje_3D (tests.myarray.test_array.TestArray) ... ERROR
test_iter (tests.myarray.test_array.TestArray) ... ok
#test_izpis_3D (tests.myarray.test_array.TestArray) ... ERROR
#test_izpis_float (tests.myarray.test_array.TestArray) ... ERROR
#test_izpis_int_negs (tests.myarray.test_array.TestArray) ... ERROR
#test_izpis_int_poz_ne_povrsti (tests.myarray.test_array.TestArray) ... ERROR
#test_izpis_int_poz_povrsti (tests.myarray.test_array.TestArray) ... ERROR
test_log (tests.myarray.test_array.TestArray) ... ok
test_mul (tests.myarray.test_array.TestArray) ... ok
test_mul_2 (tests.myarray.test_array.TestArray) ... ok
test_multi_ind_iterator (tests.myarray.test_array.TestArray) ... ok
test_multi_to_lin_ind (tests.myarray.test_array.TestArray) ... ok
test_napacne_dimenzije (tests.myarray.test_array.TestArray) ... ok
test_reversed (tests.myarray.test_array.TestArray) ... ok
test_shape (tests.myarray.test_array.TestArray) ... ok
test_shape_to_steps (tests.myarray.test_array.TestArray) ... ok
test_sub (tests.myarray.test_array.TestArray) ... ok
test_sub_2 (tests.myarray.test_array.TestArray) ... ok

Ran 35 tests in 0.019s

FAILED (errors=9)
======================================================================
```


Drugo poročilo
=

Da so testi ponovno delali sem moral spremeni sledeče: 

- V testih iskanja sem povečal indekse pričakovanega izhoda za 1
- V vsakem for i in range(...) sem indeks povečal za 1
Primer spremembe:

```py
def izpis(self):
        dimenzije = len(self.shape)
        """
        shape = (2,3)
        1 2 3
        4 5 6
        """
        print()
        if dimenzije == 2:
            if self.dtype is float:
                for i in range(self.shape[0]):
                    for j in range(self.shape[1]):
                        print("{:5.2f}".format(self[i+1, j+1]), end='') # SPREMEMBA
                    print()
            else:
                for i in range(self.shape[0]):
                    for j in range(self.shape[1]):
                        print("{:5d}".format(self[i+1, j+1]), end='') # SPREMEMBA
                    print()

        elif dimenzije == 3:
            if self.dtype is int:
                for i in range(self.shape[0]):
                    print("\nLayer", i, ":")
                    for j in range(self.shape[1]):
                        for k in range(self.shape[2]):
                            print("{:5d}".format(self[i+1, j+1, k+1]), end='') # SPREMEMBA
                        print()

        else:
            raise Exception(
                f'Array should be 2D or 3D! Your array is {dimenzije}D')
        return
```

In odsek funkcije za iskanje
```py
    def iskanje(self, isk):
        print()
        found = []
        dimenzije = len(self.shape)
        if dimenzije is 1:
            for i in range(len(self.__data)):
                if self[i+1] == isk: # SPREMEMBA
                    found.append(i+1) # SPREMEMBA
            if len(found) > 0:
                print("Found at indexses: ", end='')
                for i in range(len(found)):
                    print(found[i], end=', ')
                print()
    ...
    ...
```

Izpis po popravljenih testih: 

```py
PS D:\School\3_Letnik\Preizkusanje\arrayop> python.exe -m unittest tests.myarray.test_array.TestArray -v
test_MergeSort_1D (tests.myarray.test_array.TestArray) ... ok
test_MergeSort_stolpci (tests.myarray.test_array.TestArray) ... ok
test_MergeSort_vrstice (tests.myarray.test_array.TestArray) ... ok
test___init__ (tests.myarray.test_array.TestArray) ... ok
test_add (tests.myarray.test_array.TestArray) ... ok
test_add_2 (tests.myarray.test_array.TestArray) ... ok
test_add_3D (tests.myarray.test_array.TestArray) ... ok
test_contains (tests.myarray.test_array.TestArray) ... ok
test_div (tests.myarray.test_array.TestArray) ... ok
test_div_2 (tests.myarray.test_array.TestArray) ... ok
test_dtype (tests.myarray.test_array.TestArray) ... ok
test_exp (tests.myarray.test_array.TestArray) ... ok
test_get_set_item (tests.myarray.test_array.TestArray) ... ok
test_is_valid_instance (tests.myarray.test_array.TestArray) ... ok
test_iskanje_1D (tests.myarray.test_array.TestArray) ... ok
test_iskanje_1D_shouldFail (tests.myarray.test_array.TestArray) ... ok
test_iskanje_2D (tests.myarray.test_array.TestArray) ... ok
test_iskanje_3D (tests.myarray.test_array.TestArray) ... ok
test_iter (tests.myarray.test_array.TestArray) ... ok
test_izpis_3D (tests.myarray.test_array.TestArray) ... ok
test_izpis_float (tests.myarray.test_array.TestArray) ... ok
test_izpis_int_negs (tests.myarray.test_array.TestArray) ... ok
test_izpis_int_poz_ne_povrsti (tests.myarray.test_array.TestArray) ... ok
test_izpis_int_poz_povrsti (tests.myarray.test_array.TestArray) ... ok
test_log (tests.myarray.test_array.TestArray) ... ok
test_mul (tests.myarray.test_array.TestArray) ... ok
test_mul_2 (tests.myarray.test_array.TestArray) ... ok
test_multi_ind_iterator (tests.myarray.test_array.TestArray) ... ok
test_multi_to_lin_ind (tests.myarray.test_array.TestArray) ... ok
test_napacne_dimenzije (tests.myarray.test_array.TestArray) ... ok
test_reversed (tests.myarray.test_array.TestArray) ... ok
test_shape (tests.myarray.test_array.TestArray) ... ok
test_shape_to_steps (tests.myarray.test_array.TestArray) ... ok
test_sub (tests.myarray.test_array.TestArray) ... ok
test_sub_2 (tests.myarray.test_array.TestArray) ... ok

----------------------------------------------------------------------
Ran 35 tests in 0.014s

OK
```

<br>
<hr>
Author: Kristjan Žagar, PRO 15. 1. 2018 