Prvi test
=

Najprej sem probal za profiliranje cProfile module


```py
PS D:\School\3_Letnik\Preizkusanje\arrayop> python.exe -m unittest tests.myarray.test_array.TestArray.test_MergeSort_Profiling -v
test_MergeSort_Profiling (tests.myarray.test_array.TestArray) ...          8499707 function calls (8099709 primitive calls) in 2.935 seconds

   Ordered by: standard name

   ncalls  tottime  percall  cumtime  percall filename:lineno(function)
        1    0.002    0.002    2.935    2.935 <string>:1(<module>)
 399999/1    2.445    0.000    2.932    2.932 basearray.py:469(merge_sort_function)
        1    0.000    0.000    2.935    2.935 {built-in method builtins.exec}
  8099705    0.487    0.000    0.487    0.000 {built-in method builtins.len}
        1    0.000    0.000    0.000    0.000 {method 'disable' of '_lsprof.Profiler' objects}


ok

----------------------------------------------------------------------
Ran 1 test in 3.341s

OK
```

Nato sem probal tudi v pycharmu njihov profiler. Izpis je bil veliko bolj obširen
kot od cProfile, ampak sem pri obeh isto stvar ugotovil. Koda je ze od zacetka zelo optimizirana ampak vseeno lahko nekaj probam

Name | Call Count | Time (ms) | Own Time (ms)
--- | --- | --- | ---
randrange |	200000 | 710 |	316
_randbelow	|200000	|394|	276
test_MergeSort_Profiling|	1|	5163|	155|
randint	|200000|	849|	139|
<method 'getrandbits' of '_random.Random' objects>|	328215|	87|	87
<method 'append' of 'list' objects>|	200911|	33|	33
<method 'bit_length' of 'int' objects>|	200028|	30|	30

Trenutna verzija merge sorta, kar bi se lahko optimiziralo edino je, da ne bi vsakic
length arraya dobival.. Kar sem opazil pri analizi funkcije ter izpisu pycharm profilnika

Drugi test
=

```py
def merge_sort_function(arr):
    if len(arr) > 1:
        mid = len(arr)//2  # Finding the mid of the array .. ce je liho je -1
        L = arr[:mid]
        R = arr[mid:]

        merge_sort_function(L)  # Sorting the first half
        merge_sort_function(R)  # Sorting the second half

        i = j = k = 0

        # Copy data to temp arrays L[] and R[]
        while i < len(L) and j < len(R):
            if L[i] < R[j]:
                arr[k] = L[i]
                i += 1
            else:
                arr[k] = R[j]
                j += 1
            k += 1

        # Checking if any element was left
        while i < len(L):
            arr[k] = L[i]
            i += 1
            k += 1

        while j < len(R):
            arr[k] = R[j]
            j += 1
            k += 1
```

nova verzija: 

```py
def merge_sort_function(arr, leng): # SPREMEMBA
    if leng > 1: # SPREMEMBA        
        mid = leng//2  # SPREMEMBA
        L = arr[:mid]
        R = arr[mid:]

        leftLeng = mid # NOVO
        rightLeng = leng - leftLeng # NOVO
        
        merge_sort_function(L, leftLeng)  
        merge_sort_function(R, rightLeng)  

        i = j = k = 0
                
        while i < leftLeng and j < rightLeng: # SPREMEMBA
            if L[i] < R[j]:
                arr[k] = L[i]
                i += 1
            else:
                arr[k] = R[j]
                j += 1
            k += 1
       
        while i < leftLeng: # SPREMEMBA
            arr[k] = L[i]
            i += 1
            k += 1

        while j < rightLeng: # SPREMEMBA
            arr[k] = R[j]
            j += 1
            k += 1
```

Pa zaženimo še enkrat profilnik

```py
PS D:\School\3_Letnik\Preizkusanje\arrayop> python.exe -m unittest tests.myarray.test_array.TestArray.test_MergeSort_Profiling -v
test_MergeSort_Profiling (tests.myarray.test_array.TestArray) ...          400003 function calls (5 primitive calls) in 1.451 seconds

   Ordered by: standard name

   ncalls  tottime  percall  cumtime  percall filename:lineno(function)
        1    0.002    0.002    1.451    1.451 <string>:1(<module>)
 399999/1    1.449    0.000    1.449    1.449 basearray.py:469(merge_sort_function)
        1    0.000    0.000    1.451    1.451 {built-in method builtins.exec}
        1    0.000    0.000    0.000    0.000 {built-in method builtins.len}
        1    0.000    0.000    0.000    0.000 {method 'disable' of '_lsprof.Profiler' objects}


ok

----------------------------------------------------------------------
Ran 1 test in 1.857s

OK
```

Statistika
=

Vsako meritev sem opravil 3x ... Časovne vrednosti so bile skoraj identične

**200k ELEMENTOV**

Pred optimizacijo [s] | Po optimizaciji [s]
--- | ---
3.341 | 1.857
3.295 | 1.844
3.301 | 1.934

**2M ELEMENTOV**

Pred optimizacijo [s] | Po optimizaciji [s]
--- | ---
38.993 | 22.319
39.141 | 23.115

Po drugem profiliranju več nisem našel, oz. ga verjetno ni, nobenega načina kako izboljšati kodo za merge sort.

<br>
<hr>
Author: Kristjan Žagar, PRO 1. 15. 2019

