# pylint: disable=line-too-long
# pylint: disable=invalid-name

from typing import Tuple
from typing import List
from typing import Union
import array
from itertools import chain
import math


class BaseArray:
    """
    Razred BaseArray, za učinkovito hranjenje in dostopanje do večdimenzionalnih podatkov. Vsi podatki so istega
    številskega tipa: int ali float.

    Pri ustvarjanju podamo obliko-dimenzije tabele. Podamo lahko tudi tip hranjenih podatkov (privzeto je int) in
    podatke za inicializacijo (privzeto vse 0).

    Primeri ustvarjanja:
    a = BaseArray((10,)) # tabela 10 vrednosti, tipa int, vse 0
    a = BaseArray((3, 3), dtype=float) # tabela 9 vrednost v 3x3 matriki, tipa float, vse 0.0
    a = BaseArray((2, 3), dtype=float, dtype=float, data=(1., 2., 1., 2., 1., 2.)) # tabela 6 vrednost v 2x3 matriki,
                                                                               #tipa float, vrednosti 1. in 2.
    a = BaseArray((2, 3, 4, 5), data=tuple(range(120))) # tabela dimenzij 2x3x4x5, tipa int, vrednosti od 0 do 120


    Do podatkov dostopamo z operatorjem []. Pri tem moramo paziti, da uporabimo indekse, ki so znotraj oblike tabele.
    Začnemo z indeksom 0, zadnji indeks pa je dolžina dimenzije -1.
     Primeri dostopanja :

    a = BaseArray((2, 3, 4, 5))
    a[0, 1, 2, 1] = 10 # nastavimo vrednost 10 na mesto 0, 1, 2, 1
    v = a[0, 0, 0, 0] # preberemo vrednost na mestu 0, 0, 0 ,0

    a[0, 3, 0, 0] # neveljaven indeks, ker je vrednost 3 enaka ali večja od dolžine dimenzije

    Velikost tabele lahko preberemo z lastnostjo 'shape', podatkovni tip pa z 'dtype'. Primer:

    a.shape # to vrne vrednost  (2, 3, 4, 5)
    a.dtype # vrne tip int

    Čez tabelo lahko iteriramo s for zanko, lahko uporabimo operatorje/ukaze 'reversed' in 'in'.

    a = BaseArray((4,2), data=(1, 2, 3, 4, 5, 6, 7, 8))
    for v in a: # for zanka izpiše vrednost od 1 do 8
        print(v)
    for v in reversed(a): # for zanka izpiše vrednost od 8 do 1
        print(v)

    2 in a # vrne True
    -1 in a # vrne False
    """
    def __init__(self, shape: Tuple[int], dtype: type = None, data: Union[Tuple, List] = None):
        """
        Create an initialize a multidimensional myarray of a selected data type.
        Init the myarray to 0.

        :param shape: tuple or list of integers, shape of constructed myarray
        :param dtype: type of data stored in myarray, float or int
        :param data: list of tuple of data values, must contain consistent types and
                     have a length that matches the shape.
        """

        _check_shape(shape)
        self.__shape = (*shape,)

        self.__steps = _shape_to_steps(shape)

        if dtype is None:
            dtype = float
        if dtype not in [int, float]:
            raise Exception('Type must by int or float.')
        self.__dtype = dtype

        n_elements = 1
        for s in self.__shape:
            n_elements *= s

        if data is None:
            if dtype == int:
                self.__data = array.array('i', [0]*n_elements)
            elif dtype == float:
                self.__data = array.array('f', [0]*n_elements)
        else:
            if len(data) != n_elements:
                raise Exception(f'shape {shape:} does not match provided data length {len(data):}')

            for d in data:
                if not isinstance(d, (int, float)):
                    raise Exception(f'provided data does not have a consistent type')
            self.__data = [d for d in data]

    @property
    def shape(self):
        """
        :return: a tuple representing the shape of the myarray.
        """
        return self.__shape

    @property
    def dtype(self):
        """
        :return: the type stored in the myarray.
        """
        return self.__dtype

    def __test_indice_in_range(self, ind):
        """
        Test if the given indice is in within the range of this myarray.
        :param ind: the indices used
        :return: True if indices are valid, False otherwise.
        """
        for ax in range(len(self.__shape)):
            if ind[ax] < 1 or ind[ax] > self.shape[ax]:
                raise Exception('indice {:}, axis {:d} out of bounds [1, {:}]'.format(ind, ax, self.__shape[ax]))

    def __getitem__(self, indice):
        """
        Return a value from this myarray.
        :param indice: indice to this myarray, integer or tuple
        :return: value at indice
        """
        if not isinstance(indice, tuple):
            indice = (indice,)

        if not _is_valid_indice(indice):
            raise Exception(f'{indice} is not a valid indice')

        if len(indice) != len(self.__shape):
            raise Exception(f'indice {indice} is not valid for this myarray')

        self.__test_indice_in_range(indice)
        key_lin = _multi_to_lin_ind(indice, self.__steps)
        return self.__data[key_lin]

    def __setitem__(self, indice, value):
        """
        Set a value in this myarray.
        :param indice: valued indice to a position, integer of tuple
        :param value: value to set
        :return: does not return
        """
        if not isinstance(indice, tuple):
            indice = (indice,)

        if not _is_valid_indice(indice):
            raise Exception(f'{indice} is not a valid indice')

        if len(indice) != len(self.__shape):
            raise Exception(f'indice {indice} is not valid for this myarray')

        self.__test_indice_in_range(indice)
        key_lin = _multi_to_lin_ind(indice, self.__steps)
        self.__data[key_lin] = value

    def __iter__(self):
        for v in self.__data:
            yield v

    def __reversed__(self):
        for v in reversed(self.__data):
            yield v

    def __contains__(self, item):
        return item in self.__data

    def izpis(self):
        dimenzije = len(self.shape)
        """
        shape = (2,3)
        1 2 3
        4 5 6
        """
        print()
        if dimenzije == 2:
            if self.dtype is float:
                for i in range(self.shape[0]):
                    for j in range(self.shape[1]):
                        print("{:5.2f}".format(self[i+1, j+1]), end='')
                    print()
            else:
                for i in range(self.shape[0]):
                    for j in range(self.shape[1]):
                        print("{:5d}".format(self[i+1, j+1]), end='')
                    print()

        elif dimenzije == 3:
            if self.dtype is int:
                for i in range(self.shape[0]):
                    print("\nLayer", i, ":")
                    for j in range(self.shape[1]):
                        for k in range(self.shape[2]):
                            print("{:5d}".format(self[i+1, j+1, k+1]), end='')
                        print()

        else:
            raise Exception(
                f'Array should be 2D or 3D! Your array is {dimenzije}D')
        return


    def mergeSort(self, vrstice):
        """
        vrstice ... if true, bo sortiralo vrstice ... if false, bo sortiralo stolpce
        """
        dimenzije = len(self.shape)

        if dimenzije == 1:
            merge_sort_function(self.__data, len(self.__data))
        elif dimenzije == 2:
            if(vrstice is False):
                Matrix = [[0 for x in range(self.shape[1])] for y in range(self.shape[0])]
                for i in range(self.shape[0]):
                    Matrix[i] = self.__data[i*self.shape[1]:i*self.shape[1] + self.shape[1]]
                flat = list(chain.from_iterable([*zip(*Matrix)]))
            else:
                flat = self.__data
            cp = []
            c = self.shape[0]
            if vrstice is False:
                c = self.shape[1]
            for i in range(c):
                a = i*self.shape[1]
                b = self.shape[1]*i + self.shape[1]
                if (vrstice is False):
                    a = i * self.shape[0]
                    b = self.shape[0] * i + self.shape[0]
                cpy = flat[a:b]
                merge_sort_function(cpy, len(cpy))
                cp.append(cpy)
            if vrstice is False:
                flat2 = list(chain.from_iterable([*zip(*cp)]))
            else:
                flat2 = list(chain.from_iterable(cp))
            self.__data = flat2
        else:
            raise Exception(f'Array should be 1D or 2D! Your array is {dimenzije}D')
        return



    def iskanje(self, isk):
        print()
        found = []
        dimenzije = len(self.shape)
        if dimenzije is 1:
            for i in range(len(self.__data)):
                if self[i+1] == isk:
                    found.append(i+1)
            if len(found) > 0:
                print("Found at indexses: ", end='')
                for i in range(len(found)):
                    print(found[i], end=', ')
                print()
            else:
                print("Not found")
        elif dimenzije is 2:
            for i in range(self.shape[0]):
                for j in range(self.shape[1]):
                    if self[i+1, j+1] == isk:
                        found.append([i+1, j+1])
            if len(found) > 0:
                print("Found at indexses: ", end='')
                for i in range(len(found)):
                    print('({},{})'.format(found[i][0], found[i][1]), end=', ')
                print()
            else:
                print("Not found")
        elif dimenzije is 3:
            for i in range(self.shape[0]):
                for j in range(self.shape[1]):
                    for k in range(self.shape[2]):
                        if self[i+1, j+1, k+1] == isk:
                            found.append([i+1, j+1, k+1])
            if len(found) > 0:
                print("Found at indexses: ", end='')
                for i in range(len(found)):
                    print('({},{},{})'.format(found[i][0], found[i][1], found[i][2]), end=', ')
                print()
            else:
                print("\nNot found")
        else:
            raise Exception(f'Array should be 1D, 2D or 3D! Your array is {dimenzije}D')
        return


    def __add__ (self, other):
        if type(other) is BaseArray:
            if self.__shape != other.__shape:
                print("\nNot same shape")
                return self
            l = [x + y for x, y in zip(self.__data, other.__data)]
            self.__data = l
            return self
        elif type(other) is int:
            self.__data = [x+other for x in self.__data]
            return self
        elif type(other) is float:
            self.__dtype = float
            self.__data = [x+other for x in self.__data]
            return self
        else:
            print("\nAddition error")
            return self


    def __radd__ (self, other):
        return other + self


    def __mul__ (self, other):
        if type(other) is BaseArray:
            if self.__shape[1] != other.__shape[0]:
                print("\nIncompatable matrices")
                return self
            iint = False
            if  self.__dtype is int and self.__dtype is int:
                iint = True
            Matrix = [[0 for x in range(self.shape[1])] for y in range(self.shape[0])]
            for i in range(self.shape[0]):
                Matrix[i] = self.__data[i*self.shape[1]:i*self.shape[1] + self.shape[1]]
            Matrix2 = [[0 for x in range(other.shape[1])] for y in range(other.shape[0])]
            for i in range(other.shape[0]):
                Matrix2[i] = other.__data[i*other.shape[1]:i*other.shape[1] + other.shape[1]]

            sh1 = self.__shape[0]
            sh2 = other.__shape[1]
            res = [[0 for x in range(sh1)] for y in range(sh2)]

            for i in range(len(Matrix)):
                for j in range(len(Matrix2[0])):
                    for k in range(len(Matrix2)):
                        res[i][j] += Matrix[i][k] * Matrix2[k][j]

            self.__data = list(chain.from_iterable(res))
            self.__shape = (sh1, sh2)
            self.__steps = (sh2, 1)
            if iint is True:
                self.__dtype = int
            return self
        elif type(other) is int:
            self.__data = [x*other for x in self.__data]
            return self
        elif type(other) is float:
            self.__dtype = float
            self.__data = [x*other for x in self.__data]
            return self
        else:
            print("\nMultiplication error")
            return self


    def __rmul__ (self, other):
        return other * self


    def __sub__ (self, other):
        if type(other) is BaseArray:
            if self.__shape != other.__shape:
                print("\nNot same shape")
                return self
            l = [x - y for x, y in zip(self.__data, other.__data)]
            self.__data = l
            return self
        elif type(other) is int:
            self.__data = [x-other for x in self.__data]
            return self
        elif type(other) is float:
            self.__dtype = float
            self.__data = [x-other for x in self.__data]
            return self
        else:
            print("\nSubtraction error")
            return self


    def __rsub__(self, other):
        return (self - other) * -1


    def __truediv__(self, other):
        if type(other) is BaseArray:
            if self.__shape != other.__shape:
                print("\nNot same shape")
                return self
            l = [x / y for x, y in zip(self.__data, other.__data)]
            self.__data = l
            return self
        elif type(other) is int:
            self.__data = [x/other for x in self.__data]
            return self
        elif type(other) is float:
            self.__dtype = float
            self.__data = [x/other for x in self.__data]
            return self
        else:
            print("\nDivision error")
            return self


    def log (self, base):
        self.__dtype = float
        self.__data = [round(math.log(x, base),2) for x in self.__data]
        return self

    def exp (self):
        self.__dtype = float
        self.__data = [round(math.exp(x),2) for x in self.__data]
        return self



def _check_shape(shape):
    if not isinstance(shape, (tuple, list)):
        raise Exception(f'shape {shape:} is not a tuple or list')
    for dim in shape:
        if not isinstance(dim, int):
            raise Exception(f'shape {shape:} contains a non integer {dim:}')


def _multi_ind_iterator(multi_inds, shape):
    inds = multi_inds[0]
    s = shape[0]

    if isinstance(inds, slice):
        start, stop, step = inds.start, inds.stop, inds.step
        if start is None:
            start = 1
        if step is None:
            step = 1
        if stop is not None:
            stop += 1
        inds_itt = range(0, s+1)[start:stop:step]
    else:  # type(inds) is int:
        inds_itt = (inds,)

    for ind in inds_itt:
        if len(shape) > 1:
            for ind_tail in _multi_ind_iterator(multi_inds[1:], shape[1:]):
                yield (ind,)+ind_tail
        else:
            yield (ind,)


def _shape_to_steps(shape):
    dim_steps_rev = []
    s = 1
    for d in reversed(shape):
        dim_steps_rev.append(s)
        s *= d
    steps = tuple(reversed(dim_steps_rev))
    return steps


def _multi_to_lin_ind(inds, steps):
    i = 0
    for n, s in enumerate(steps):
        i += (inds[n]-1)*s
    return i


def _is_valid_indice(indice):
    if isinstance(indice, tuple):
        if not indice:
            return False
        for i in indice:
            if not isinstance(i, int):
                return False
    elif not isinstance(indice, int):
        return False
    return True


def merge_sort_function(arr, leng): # SPREMEMBA
    if leng > 1: # SPREMEMBA
        mid = leng//2  # SPREMEMBA
        L = arr[:mid]
        R = arr[mid:]

        leftLeng = mid # NOVO
        rightLeng = leng - leftLeng # NOVO

        merge_sort_function(L, leftLeng)
        merge_sort_function(R, rightLeng)

        i = j = k = 0

        while i < leftLeng and j < rightLeng:
            if L[i] < R[j]:
                arr[k] = L[i]
                i += 1
            else:
                arr[k] = R[j]
                j += 1
            k += 1

        while i < leftLeng:
            arr[k] = L[i]
            i += 1
            k += 1

        while j < rightLeng:
            arr[k] = R[j]
            j += 1
            k += 1



