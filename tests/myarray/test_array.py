from unittest import TestCase
from myarray.basearray import BaseArray
import myarray.basearray as ndarray
import io
import sys
import math
import unittest
import random
import cProfile
from myarray.basearray import merge_sort_function


class TestArray(TestCase):
    def test___init__(self):

        # simple init tests, other things are tester later
        try:
            a = BaseArray((2,))
            a = BaseArray([2, 5])
            a = BaseArray((2, 5), dtype=int)
            a = BaseArray((2, 5), dtype=float)
        except Exception as e:
            self.fail(f'basic constructor test failed, with exception {e:}')

        # test invalid parameters as shape
        with self.assertRaises(Exception) as cm:
            a = BaseArray(5)
        self.assertEqual(('shape 5 is not a tuple or list',),
                         cm.exception.args)

        # test invalid value in shape
        with self.assertRaises(Exception) as cm:
            a = BaseArray((4, 'a'))
        self.assertEqual(('shape (4, \'a\') contains a non integer a',),
                         cm.exception.args)

        # TODO test invalid dtype

        # test valid data param, list, tuple
        a = BaseArray((3, 4), dtype=int, data=list(range(12)))
        a = BaseArray((3, 4), dtype=int, data=tuple(range(12)))

        # test invalid data param
        # invalid data length
        with self.assertRaises(Exception) as cm:
            a = BaseArray((2, 4), dtype=int, data=list(range(12)))
        self.assertEqual(('shape (2, 4) does not match provided data length 12',),
                         cm.exception.args)

        # inconsistent data type
        with self.assertRaises(Exception) as cm:
            a = BaseArray((2, 2), dtype=int, data=(4, 'a', (), 2.0))
        self.assertEqual(('provided data does not have a consistent type',),
                         cm.exception.args)

    def test_shape(self):
        a = BaseArray((5,))
        self.assertTupleEqual(a.shape, (5,))
        a = BaseArray((2, 5))
        self.assertTupleEqual(a.shape, (2, 5))
        a = BaseArray((3, 2, 5))
        self.assertEqual(a.shape, (3, 2, 5))

    def test_dtype(self):
        # test if setting a type returns the set type
        a = BaseArray((1,))
        self.assertIsInstance(a[1], float)
        a = BaseArray((1,), int)
        self.assertIsInstance(a[1], int)
        a = BaseArray((1,), float)
        self.assertIsInstance(a[1], float)

    def test_get_set_item(self):
        # test if setting a value returns the value
        a = BaseArray((1,))
        a[1] = 1
        self.assertEqual(1, a[1])
        # test for multiple dimensions
        a = BaseArray((2, 1))
        a[1, 1] = 1
        a[2, 1] = 1
        self.assertEqual(1, a[1, 1])
        self.assertEqual(1, a[2, 1])

        # test with invalid indices
        a = BaseArray((2, 2))
        with self.assertRaises(Exception) as cm:
            a[4, 1] = 0
        self.assertEqual(('indice (4, 1), axis 0 out of bounds [1, 2]',),
                         cm.exception.args)

        # test invalid type of ind
        with self.assertRaises(Exception) as cm:
            a[1, 1.1] = 0
        self.assertEqual(cm.exception.args,
                         ('(1, 1.1) is not a valid indice',))

        with self.assertRaises(Exception) as cm:
            a[1, 'a'] = 0
        self.assertEqual(('(1, \'a\') is not a valid indice',),
                         cm.exception.args)

        # test invalid number of ind
        with self.assertRaises(Exception) as cm:
            a[2] = 0
        self.assertEqual(('indice (2,) is not valid for this myarray',),
                         cm.exception.args)

        # test data intialized via data parameter
        a = BaseArray((2, 3, 4), dtype=int, data=tuple(range(24)))
        self.assertEqual(3, a[1, 1, 4])
        self.assertEqual(13, a[2, 1, 2])

        # TODO enter invalid type


    def test_iter(self):
        a = BaseArray((2, 3), dtype=int)
        a[1, 1] = 1
        a[1, 2] = 2
        a[1, 3] = 3
        a[2, 1] = 4
        a[2, 2] = 5
        a[2, 3] = 6

        a_vals = [v for v in a]
        self.assertListEqual([1, 2, 3, 4, 5, 6], a_vals)

    def test_reversed(self):
        a = BaseArray((2, 3), dtype=int)
        a[1, 1] = 1
        a[1, 2] = 2
        a[1, 3] = 3
        a[2, 1] = 4
        a[2, 2] = 5
        a[2, 3] = 6

        a_vals = [v for v in reversed(a)]
        self.assertListEqual([6, 5, 4, 3, 2, 1], a_vals)

    def test_contains(self):
        a = BaseArray((1,), dtype=float)
        a[1] = 1

        self.assertIn(1., a)
        self.assertIn(1, a)
        self.assertNotIn(0, a)

    def test_shape_to_steps(self):
        shape = (4, 2, 3)
        steps_exp = (6, 3, 1)
        steps_res = ndarray._shape_to_steps(shape)
        self.assertTupleEqual(steps_exp, steps_res)

        shape = [1]
        steps_exp = (1,)
        steps_res = ndarray._shape_to_steps(shape)
        self.assertTupleEqual(steps_exp, steps_res)

        shape = []
        steps_exp = ()
        steps_res = ndarray._shape_to_steps(shape)
        self.assertTupleEqual(steps_exp, steps_res)

    def test_multi_to_lin_ind(self):
        # shape = 4, 2,
        # 0 1
        # 2 3
        # 4 5
        # 6 7
        steps = 2, 1
        multi_lin_inds_pairs = (((1, 1), 0),
                                ((2, 2), 3),
                                ((4, 1), 6),
                                ((4, 2), 7))
        for multi_inds, lin_ind_exp in multi_lin_inds_pairs:
            lin_ind_res = ndarray._multi_to_lin_ind(multi_inds, steps)
            self.assertEqual(lin_ind_exp, lin_ind_res)

        # shape = 2, 3, 4
        #
        # 0 1 2 3
        # 4 5 6 7
        #
        #  8  9 10 11
        # 12 13 14 15
        steps = 8, 4, 1
        multi_lin_inds_pairs = (((1, 1, 1), 0),
                                ((2, 2, 1), 12),
                                ((1, 1, 4), 3),
                                ((2, 2, 3), 14))

        for multi_inds, lin_ind_exp in multi_lin_inds_pairs:
            lin_ind_res = ndarray._multi_to_lin_ind(multi_inds, steps)
            self.assertEqual(lin_ind_res, lin_ind_exp)

    def test_is_valid_instance(self):
        self.assertTrue(ndarray._is_valid_indice(2))
        self.assertTrue(ndarray._is_valid_indice((2, 1)))

        self.assertFalse(ndarray._is_valid_indice(()))
        self.assertFalse(ndarray._is_valid_indice([2]))
        self.assertFalse(ndarray._is_valid_indice(2.0))
        self.assertFalse(ndarray._is_valid_indice((2, 3.)))

    def test_multi_ind_iterator(self):
        shape = (30,)
        ind = (slice(10, 20, 3),)
        ind_list_exp = ((10,),
                        (13,),
                        (16,),
                        (19,))
        ind_list_res = tuple(ndarray._multi_ind_iterator(ind, shape))
        for ind_exp, ind_res in zip(ind_list_exp, ind_list_res):
            self.assertTupleEqual(ind_exp, ind_res)

        shape = (5, 5, 5)
        ind = (2, 2, 2)
        ind_list_exp = ((2, 2, 2),)
        ind_list_res = tuple(ndarray._multi_ind_iterator(ind, shape))
        for ind_exp, ind_res in zip(ind_list_exp, ind_list_res):
            self.assertTupleEqual(ind_exp, ind_res)

        ind = (2, slice(3), 2)
        ind_list_exp = ((2, 1, 2),
                        (2, 2, 2),
                        (2, 3, 2),
                        )
        ind_list_res = tuple(ndarray._multi_ind_iterator(ind, shape))
        for ind_exp, ind_res in zip(ind_list_exp, ind_list_res):
            self.assertTupleEqual(ind_exp, ind_res)

        ind = (slice(None, None, 2), slice(3), slice(2, 4))
        ind_list_exp = ((1, 1, 2), (1, 1, 3), (1, 1, 4),
                        (1, 2, 2), (1, 2, 3), (1, 2, 4),
                        (1, 3, 2), (1, 3, 3), (1, 3, 4),
                        (3, 1, 2), (3, 1, 3), (3, 1, 4),
                        (3, 2, 2), (3, 2, 3), (3, 2, 4),
                        (3, 3, 2), (3, 3, 3), (3, 3, 4),
                        (5, 1, 2), (5, 1, 3), (5, 1, 4),
                        (5, 2, 2), (5, 2, 3), (5, 2, 4),
                        (5, 3, 2), (5, 3, 3), (5, 3, 4),
                        )
        ind_list_res = tuple(ndarray._multi_ind_iterator(ind, shape))
        for ind_exp, ind_res in zip(ind_list_exp, ind_list_res):
            self.assertTupleEqual(ind_exp, ind_res)

    def test_izpis_int_poz_povrsti(self):
        a = BaseArray((2, 3), data=(1, 2, 3, 4, 5, 6), dtype=int)
        exp = '\n{:5d}{:5d}{:5d}\n{:5d}{:5d}{:5d}\n'.format(1, 2, 3, 4, 5, 6)

        capturedOutput = io.StringIO()  # Create StringIO object
        sys.stdout = capturedOutput  # and redirect stdout.
        BaseArray.izpis(a)  # Call function.
        sys.stdout = sys.__stdout__  # Reset redirect.

        self.assertMultiLineEqual(capturedOutput.getvalue(), exp)

    def test_izpis_int_poz_ne_povrsti(self):
        a = BaseArray((2, 3), data=(5, 1, 4, 3, 2, 6), dtype=int)
        exp = '\n{:5d}{:5d}{:5d}\n{:5d}{:5d}{:5d}\n'.format(5, 1, 4, 3, 2, 6)

        capturedOutput = io.StringIO()  # Create StringIO object
        sys.stdout = capturedOutput  # and redirect stdout.
        BaseArray.izpis(a)  # Call function.
        sys.stdout = sys.__stdout__  # Reset redirect.

        self.assertMultiLineEqual(capturedOutput.getvalue(), exp)

    def test_izpis_int_negs(self):
        a = BaseArray((2, 3), data=(-3, -2, -1, 0, 1, 2), dtype=int)
        exp = '\n{:5d}{:5d}{:5d}\n{:5d}{:5d}{:5d}\n'.format(
            -3, -2, -1, 0, 1, 2)

        capturedOutput = io.StringIO()  # Create StringIO object
        sys.stdout = capturedOutput  # and redirect stdout.
        BaseArray.izpis(a)  # Call function.
        sys.stdout = sys.__stdout__  # Reset redirect.

        self.assertMultiLineEqual(capturedOutput.getvalue(), exp)

    def test_izpis_float(self):
        a = BaseArray((2, 3), data=(1.2, 2, 3.4, 4, 5, 6), dtype=float)
        exp = '\n{:5.2f}{:5.2f}{:5.2f}\n{:5.2f}{:5.2f}{:5.2f}\n'.format(
            1.2, 2, 3.4, 4, 5, 6)

        capturedOutput = io.StringIO()  # Create StringIO object
        sys.stdout = capturedOutput  # and redirect stdout.
        BaseArray.izpis(a)  # Call function.
        sys.stdout = sys.__stdout__  # Reset redirect.

        self.assertEqual(capturedOutput.getvalue(), exp)

    def test_izpis_3D(self):
        a = BaseArray((3, 3, 3), dtype=int)
        exp = '\n\nLayer 0 :\n{:5d}{:5d}{:5d}\n{:5d}{:5d}{:5d}\n{:5d}{:5d}{:5d}\n'.format(
            0, 0, 0, 0, 0, 0, 0, 0, 0)
        exp += '\nLayer 1 :\n{:5d}{:5d}{:5d}\n{:5d}{:5d}{:5d}\n{:5d}{:5d}{:5d}\n'.format(
            0, 0, 0, 0, 0, 0, 0, 0, 0)
        exp += '\nLayer 2 :\n{:5d}{:5d}{:5d}\n{:5d}{:5d}{:5d}\n{:5d}{:5d}{:5d}\n'.format(
            0, 0, 0, 0, 0, 0, 0, 0, 0)

        capturedOutput = io.StringIO()  # Create StringIO object
        sys.stdout = capturedOutput  # and redirect stdout.
        BaseArray.izpis(a)  # Call function.
        sys.stdout = sys.__stdout__  # Reset redirect.

        self.assertEqual(capturedOutput.getvalue(), exp)

    def xtest_MergeSort_Profiling(self):
        elements = 2000000
        max = 100000
        a = []
        for x in range(elements):
            a.append(random.randint(1, max))

        cProfile.runctx('merge_sort_function(a, len(a))', globals(), locals())
        self.assertTrue(True)

    def test_MergeSort_1D(self):
        a = BaseArray((8,), data=(50, 31, 17, 37, 18, 54, 63, 36))
        aCpy = [17, 18, 31, 36, 37, 50, 54, 63]
        BaseArray.mergeSort(a, True)
        self.assertListEqual(aCpy, [v for v in a])

    def test_MergeSort_stolpci(self):
        a = BaseArray((2, 3), data=(5, 1, 4, 3, 2, 6), dtype=int)
        aCpy = [3, 1, 4, 5, 2, 6]
        BaseArray.mergeSort(a, False)
        self.assertListEqual(aCpy, [v for v in a])

    def test_MergeSort_vrstice(self):
        b = BaseArray((2, 3), data=(5, 1, 4, 3, 2, 6), dtype=int)
        bCpy = [1, 4, 5, 2, 3, 6]
        BaseArray.mergeSort(b, True)
        self.assertListEqual(bCpy, [v for v in b])

    def test_iskanje_1D(self):
        a = BaseArray((6,), data=(1, 2, 1, 2, 3, 1))
        exp = '\nFound at indexses: {}, {}, {}, \n'.format(1, 3, 6)

        capturedOutput = io.StringIO()
        sys.stdout = capturedOutput
        BaseArray.iskanje(a, 1)
        sys.stdout = sys.__stdout__

        self.assertEqual(capturedOutput.getvalue(), exp)

    def test_iskanje_2D(self):
        a = BaseArray((2, 3), data=(1, 2, 1, 2, 3, 1))
        exp = '\nFound at indexses: ({},{}), ({},{}), ({},{}), \n'.format(
            1, 1, 1, 3, 2, 3)

        capturedOutput = io.StringIO()
        sys.stdout = capturedOutput
        BaseArray.iskanje(a, 1)
        sys.stdout = sys.__stdout__

        self.assertEqual(capturedOutput.getvalue(), exp)

    def test_iskanje_3D(self):
        a = BaseArray((2, 2, 2), data=(1, 2, 4, 3, 1, 1, 2, 3))
        exp = '\nFound at indexses: ({},{},{}), ({},{},{}), ({},{},{}), \n'.format(
            1, 1, 1, 2, 1, 1, 2, 1, 2)

        capturedOutput = io.StringIO()
        sys.stdout = capturedOutput
        BaseArray.iskanje(a, 1)
        sys.stdout = sys.__stdout__

        self.assertEqual(capturedOutput.getvalue(), exp)

    def test_iskanje_1D_shouldFail(self):
        a = BaseArray((6,), data=(1, 2, 1, 2, 3, 1))
        exp = '\nNot found\n'

        capturedOutput = io.StringIO()
        sys.stdout = capturedOutput
        BaseArray.iskanje(a, 10)
        sys.stdout = sys.__stdout__

        self.assertEqual(capturedOutput.getvalue(), exp)

    def test_add(self):
        a = BaseArray((2, 3), data=(1, 2, 1, 2, 3, 1))
        b = a + 1
        self.assertListEqual([v for v in b], [2, 3, 2, 3, 4, 2])

    def test_sub(self):
        a = BaseArray((2, 3), data=(1, 2, 1, 2, 3, 1))
        b = a - 1
        self.assertListEqual([v for v in b], [0, 1, 0, 1, 2, 0])

    def test_div(self):
        a = BaseArray((2, 3), data=(1, 2, 1, 2, 3, 1))
        b = a / 2
        self.assertListEqual([v for v in b],
                             [0.50, 1.00, 0.50, 1.00, 1.50, 0.50])

    def test_mul(self):
        a = BaseArray((2, 3), data=(1, 2, 1, 2, 3, 1), dtype=int)
        b = a * 2
        self.assertListEqual([v for v in b], [2, 4, 2, 4, 6, 2])

    def test_add_2(self):
        a = BaseArray((2, 3), data=(1, 2, 1, 2, 3, 1))
        a1 = BaseArray((2, 3), data=(5, 2, 1, 2, 3, 1), dtype=int)
        c = a + a1
        self.assertListEqual([v for v in c], [6, 4, 2, 4, 6, 2])

    def test_sub_2(self):
        a = BaseArray((2, 3), data=(1, 2, 1, 2, 3, 1))
        a1 = BaseArray((2, 3), data=(5, 2, 1, 2, 3, 1), dtype=int)
        c = a - a1
        self.assertListEqual([v for v in c], [-4, 0, 0, 0, 0, 0])

    def test_div_2(self):
        a = BaseArray((2, 3), data=(1, 2, 1, 2, 3, 1))
        a1 = BaseArray((2, 3), data=(5, 2, 1, 2, 3, 1), dtype=int)
        c = a / a1
        self.assertListEqual([v for v in c],
                             [0.20, 1.00, 1.00, 1.00, 1.00, 1.00])

    def test_mul_2(self):
        a = BaseArray((2, 3), data=(1, 2, 1, 2, 3, 1), dtype=int)
        a1 = BaseArray((3, 2), data=(5, 2, 1, 2, 3, 1), dtype=int)
        a *= 2
        c = a * a1
        self.assertListEqual([v for v in c], [20, 14, 32, 22])

    def test_napacne_dimenzije(self):
        a = BaseArray((2, 3), data=(1, 2, 1, 2, 3, 1), dtype=int)
        a1 = BaseArray((3, 2), data=(5, 2, 1, 2, 3, 1), dtype=int)
        exp = '\nNot same shape\n'

        capturedOutput = io.StringIO()
        sys.stdout = capturedOutput
        b = a + a1
        sys.stdout = sys.__stdout__

        self.assertEqual(capturedOutput.getvalue(), exp)

    def test_add_3D(self):
        a = BaseArray((2, 2, 2), data=(1, 2, 1, 2, 3, 1, 1, 1))
        b = a + 1
        self.assertListEqual([v for v in b], [2, 3, 2, 3, 4, 2, 2, 2])

    def test_log(self):
        a = BaseArray((2, 3), data=(1, 2, 1, 2, 3, 1), dtype=int)
        BaseArray.log(a, math.e)
        self.assertListEqual([v for v in a],
                             [0.00, 0.69, 0.00, 0.69, 1.1, 0.00])

    def test_exp(self):
        a = BaseArray((2, 3), data=(1, 2, 1, 2, 1, 1), dtype=int)
        BaseArray.exp(a)
        self.assertListEqual([v for v in a],
                             [2.72, 7.39, 2.72, 7.39, 2.72, 2.72])

    def test_izpisi(self):
        tests = [
            'test_izpis_int_poz_povrsti',
            'test_izpis_int_poz_ne_povrsti',
            'test_izpis_int_negs',
            'test_izpis_float',
            'test_izpis_3D'
        ]
        s = unittest.TestSuite()
        s.addTests(map(TestArray, tests))
        return s

    def test_sortiranje(self):
        tests = [
            'test_MergeSort_1D',
            'test_MergeSort_stolpci',
            'test_MergeSort_vrstice'
        ]
        s = unittest.TestSuite()
        s.addTests(map(TestArray, tests))
        return s

    def test_iskanje(self):
        tests = [
            'test_iskanje_1D',
            'test_iskanje_2D',
            'test_iskanje_3D',
            'test_iskanje_1D_shouldFail'
        ]
        s = unittest.TestSuite()
        s.addTests(map(TestArray, tests))
        return s

    def test_matOperacije(self):
        tests = [
            'test_add',
            'test_sub',
            'test_div',
            'test_mul',
            'test_add_2',
            'test_sub_2',
            'test_div_2',
            'test_mul_2',
            'test_napacne_dimenzije',
            'test_add_3D',
            'test_log',
            'test_exp'
        ]
        s = unittest.TestSuite()
        s.addTests(map(TestArray, tests))
        return s
